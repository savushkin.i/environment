#!/usr/bin/env bash

set -e

EXECUTABLE_FULL_PATH="/usr/bin/hadolint"

wget --no-verbose --output-document="${EXECUTABLE_FULL_PATH}" "https://github.com/hadolint/hadolint/releases/download/v${HADOLINT_VER}/hadolint-Linux-x86_64"

chmod +x "$EXECUTABLE_FULL_PATH"

hadolint --version
