#!/usr/bin/env bash

set -e

mkdir -p /opt/semgrep

cd /opt/semgrep

python3 -m venv env

source /opt/semgrep/env/bin/activate

python3 -m pip install "semgrep==${SEMGREP_VER}"

echo "semgrep version: $(semgrep --version)"

deactivate
