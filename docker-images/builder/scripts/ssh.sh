#!/usr/bin/env bash

set -e

mkdir -p ~/.ssh
:> ~/.ssh/known_hosts
chmod 700 ~/.ssh
chmod 644 ~/.ssh/known_hosts
