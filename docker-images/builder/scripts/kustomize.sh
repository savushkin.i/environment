#!/usr/bin/env bash

set -e

EXECUTABLE_FULL_PATH="/usr/bin/kustomize"

ARCHIVE="kustomize_v${KUSTOMIZE_VER}_linux_amd64.tar.gz"

wget --no-verbose --output-document="${ARCHIVE}" "https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VER}/kustomize_v${KUSTOMIZE_VER}_linux_amd64.tar.gz"

tar xzf "$ARCHIVE"

rm -f "$ARCHIVE"

mv ./kustomize "${EXECUTABLE_FULL_PATH}"

chmod +x "$EXECUTABLE_FULL_PATH"

kustomize version
