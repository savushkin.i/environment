#!/usr/bin/env bash

set -e

gem install bundler
bundle config build.nokogiri --use-system-libraries
bundle config git.allow_insecure true
bundle config set --global deployment 'true'
gem install json
gem cleanup

echo "ruby version: $(ruby -v)}"
echo "gem version: $(gem -v)}"
echo "bundler version: $(bundle -v)}"
