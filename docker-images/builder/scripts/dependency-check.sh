#!/usr/bin/env bash

set -e

ARCHIVE="dependency-check-${DEPENDENCY_CHECK_VER}-release.zip"

wget --no-verbose --output-document="/opt/${ARCHIVE}" "https://github.com/jeremylong/DependencyCheck/releases/download/v${DEPENDENCY_CHECK_VER}/${ARCHIVE}"

unzip "/opt/${ARCHIVE}" -d "/opt/"
mv /tmp/dependency-check/* "/opt/dependency-check/"
rm -f "/opt/${ARCHIVE}"

ln -s /opt/dependency-check/bin/dependency-check.sh /usr/bin/dependency-check

dependency-check --updateonly

echo "dependency-check version: $(dependency-check --version)"
