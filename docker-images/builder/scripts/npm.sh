#!/usr/bin/env bash

set -e

npm install "npm@${NPM_VER}" --global

echo "npm version: $(npm --version)"
echo "node version: $(node --version)"
