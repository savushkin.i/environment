#!/usr/bin/env bash

set -e

mkdir -p /opt/trufflehog3

cd /opt/trufflehog3

python3 -m venv env

source /opt/trufflehog3/env/bin/activate

python3 -m pip install -i https://pypi.org/simple "trufflehog3==${TRUFFLEHOG3_VER}"

echo "trufflehog3 version: $(trufflehog3 --version)"

deactivate
