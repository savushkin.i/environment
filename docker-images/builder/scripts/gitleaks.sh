#!/usr/bin/env bash

set -e

EXECUTABLE_FULL_PATH="/usr/bin/gitleaks"

ARCHIVE="gitleaks_${GITLEAKS_VER}_linux_x64.tar.gz"

wget --no-verbose --output-document="${ARCHIVE}" "https://github.com/zricethezav/gitleaks/releases/download/v${GITLEAKS_VER}/gitleaks_${GITLEAKS_VER}_linux_x64.tar.gz"

tar xzf "$ARCHIVE"

rm -f "$ARCHIVE"

mv ./gitleaks "${EXECUTABLE_FULL_PATH}"

chmod +x "$EXECUTABLE_FULL_PATH"

echo "gitleaks version: $(gitleaks version)"
