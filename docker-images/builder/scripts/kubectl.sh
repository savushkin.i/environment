#!/usr/bin/env bash

set -e

EXECUTABLE_FULL_PATH="/usr/bin/kubectl"

wget --no-verbose --output-document="${EXECUTABLE_FULL_PATH}" "https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VER}/bin/linux/amd64/kubectl"

chmod +x "$EXECUTABLE_FULL_PATH"

mkdir -p ~/.kube

kubectl version --client
