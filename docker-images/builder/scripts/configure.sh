#!/usr/bin/env bash

set -e

apt-get update
apt-get install --yes \
apt-transport-https \
gnupg \
ca-certificates \
ca-certificates-java \
curl \
wget \
apt-utils \
software-properties-common \
zip \
unzip \
jq \
git \
openssh-client \
gettext \
make \
gcc \
g++ \
build-essential \
locales \
python3 \
python3-dev \
python3-pip \
python3-venv \
openjdk-17-jre-headless \
maven \
ruby-full

RELEASE=$(lsb_release -rs)
CODENAME=$(lsb_release -cs)

# Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/trusted.gpg.d/docker.gpg
add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu ${CODENAME} stable"
apt-get update
apt-get install --yes docker-ce="5:${DOCKER_VER}~3-0~ubuntu-${CODENAME}" docker-ce-cli="5:${DOCKER_VER}~3-0~ubuntu-${CODENAME}" containerd.io docker-compose-plugin

# Dotnet
curl -O -L -s "https://packages.microsoft.com/config/ubuntu/${RELEASE}/packages-microsoft-prod.deb"
dpkg -i packages-microsoft-prod.deb
apt-get update
apt-get install --yes dotnet-sdk-6.0
rm packages-microsoft-prod.deb

# NodeJS
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/trusted.gpg.d/node.gpg
add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://deb.nodesource.com/node_20.x nodistro main"
apt-get update
apt-get install --yes nodejs yarn

python3 -m pip install --user virtualenv

/tmp/scripts/ssh.sh
/tmp/scripts/npm.sh
/tmp/scripts/kubectl.sh
/tmp/scripts/kustomize.sh
/tmp/scripts/hadolint.sh
/tmp/scripts/gitleaks.sh
/tmp/scripts/semgrep.sh
/tmp/scripts/trufflehog3.sh
/tmp/scripts/dependency-check.sh
/tmp/scripts/ruby.sh

apt-get clean
apt-get autoremove --yes

rm -rf /usr/lib/ruby/gems/*/cache/* /var/lib/apt/lists/* /usr/share/man /tmp/*
