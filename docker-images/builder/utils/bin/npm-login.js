#!/usr/bin/env node

'use strict';

import {existsSync, readFileSync, writeFileSync} from 'fs';
import {EOL, homedir} from 'os';
import {execSync} from 'child_process';

const {
    NPM_TOKEN,
    NPM_USERNAME,
    NPM_PASSWORD,
    NPM_EMAIL
} = process.env;

function isDefined(val) {
    return val !== undefined && val !== null && val !== '';
}

const systemConfigPath = `${homedir()}/.npmrc`;
const localConfigPath = `${process.cwd()}/.npmrc`;

const prefix = /^(@.+:)?registry = /;

const isLegacyAuth = isDefined(NPM_EMAIL) && isDefined(NPM_USERNAME) && isDefined(NPM_PASSWORD);
const isTokenAuth = isDefined(NPM_TOKEN);

if ((isLegacyAuth || isTokenAuth) && existsSync(localConfigPath)) {
    console.log(isLegacyAuth
        ? 'NPM_EMAIL, NPM_USERNAME and NPM_PASSWORD environment variables found\nUse legacy auth'
        : 'NPM_TOKEN variable found\nUse token auth'
    );

    const registries = [
        ...new Set(
            readFileSync(localConfigPath).toString()
                .split(EOL)
                .filter((registry) => registry.match(prefix))
                .map((registry) => registry.replace(prefix, ''))
        )
    ];

    const registriesWithAuth = registries
        .map((registry) => registry.replace(/^https?:/, ''))
        .map(
            (registry) =>
                isLegacyAuth
                    ? `${registry}:_auth=${Buffer.from(`${NPM_USERNAME}:${NPM_PASSWORD}`).toString('base64')}`
                    : `${registry}:_authToken=${NPM_TOKEN}`
        );

    const content = isLegacyAuth
        ? `email = ${NPM_EMAIL}${EOL}${registriesWithAuth.join(EOL)}${EOL}`
        : `${registriesWithAuth.join(EOL)}${EOL}`;

    writeFileSync(systemConfigPath, content);

    registries.forEach((registry) => {
        const command = `npm whoami --registry ${registry}`;
        console.log(`Check auth for ${registry} registry`);
        console.log(command);
        try {
            console.log(
                execSync(command, {
                    env: {...process.env}
                })
                    .toString()
            );
        } catch {

        }
    });
} else {
    console.log('NPM_EMAIL, NPM_USERNAME, NPM_PASSWORD or NPM_TOKEN environment variables not found');
}
