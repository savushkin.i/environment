#!/usr/bin/env node

'use strict';

import { importScan } from '../lib/defect-dojo-client.js';

try {
    importScan.import()
        .then(({data: importScan}) => console.log(importScan))
        .catch((error) => {
            if (error && error.response && error.response.data) {
                console.log(error.response.data);
            }

            return Promise.resolve({data: undefined, error});
        });
} catch (err) {
    console.error('Error: %O', err);
}
