#!/usr/bin/env node

'use strict';

import {writeFileSync} from 'fs';
import {EOL} from 'os';
import {readFile} from 'fs/promises';

const working_directory = process.cwd();

const {
    CI_REPOSITORY_URL,
    CI_COMMIT_REF_NAME
} = process.env;

const { default: semanticRelease } = await import(`${working_directory}/node_modules/semantic-release/index.js`);

const { version } = JSON.parse(await readFile(`${working_directory}/package.json`, 'utf8'));

const preset = 'conventionalcommits';

try {
    Promise.all([
        semanticRelease({
            repositoryUrl: CI_REPOSITORY_URL,
            branches: [
                {
                    name: CI_COMMIT_REF_NAME
                }
            ],
            plugins: [
                ['@semantic-release/commit-analyzer', {
                    'preset': preset,
                    'releaseRules': [
                        {'type': 'build', 'scope': 'npm', 'release': 'patch'},
                        {'type': 'build', 'scope': 'deps', 'release': 'patch'},
                        {'type': 'chore', 'scope': 'deps', 'release': 'patch'}
                    ]
                }]
            ],
            dryRun: true
        }, {})
    ])
        .then(([release]) => {
            const env = [];

            if (release) {
                const {lastRelease, nextRelease} = release;

                if (lastRelease.version) {
                    console.log(`The last release was "${lastRelease.version}".`);
                    env.push(`PROJECT_VERSION=${lastRelease.version}`);
                }

                if (nextRelease.version) {
                    console.log(`The next release was "${nextRelease.version}".`);
                    env.push(`PROJECT_NEXT_VERSION=${nextRelease.version}`);
                }
            } else {
                console.log('No release published.');
                env.push(`PROJECT_VERSION=${version}`);
                env.push(`PROJECT_NEXT_VERSION=${version}`);
            }

            console.log(env);

            writeFileSync('./project.env', `${env.join(EOL)}${EOL}`);
        });

} catch (err) {
    console.error('Error: %O', err);
}
