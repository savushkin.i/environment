'use strict';

import axios from 'axios';
import FormData from 'form-data';
import { createReadStream } from 'fs';

const working_directory = process.cwd();

const {
    CI_PROJECT_URL,
    CI_COMMIT_REF_NAME,
    CI_COMMIT_SHORT_SHA,
    CI_COMMIT_AUTHOR,
    CI_PIPELINE_ID,
    SEC_DD_URL,
    SEC_DD_KEY,
    SEC_DD_PRODUCT_ID,
    SEC_DD_ENGAGEMENT_ID,
    SEC_DD_SCAN_TYPE,
    SEC_DD_REPORT_FILE_NAME,
    SEC_DD_GROUP_BY
} = process.env;

const instance = axios.create({
    baseURL: SEC_DD_URL,
    headers: {
        'Authorization': `Token ${SEC_DD_KEY}`,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    },
    maxContentLength: Infinity,
    maxBodyLength: Infinity
});

const now = new Date().toISOString()
    .split('T')
    .shift();

export const client = instance;
export const engagements = {
    create: () => instance.post('/api/v2/engagements/', {
        target_start: now,
        target_end: now,
        product: SEC_DD_PRODUCT_ID,
        name: CI_PIPELINE_ID,
        build_id: CI_PIPELINE_ID,
        commit_hash: CI_COMMIT_SHORT_SHA,
        description: `Latest commit by ${CI_COMMIT_AUTHOR}`,
        source_code_management_uri: `${CI_PROJECT_URL}/-/tree/${CI_COMMIT_REF_NAME}`,
        engagement_type: 'CI/CD'
    })
};
export const importScan = {
    import: () => {
        const data = new FormData();

        data.append('file', createReadStream(`${working_directory}/${SEC_DD_REPORT_FILE_NAME}`));
        data.append('scan_type', SEC_DD_SCAN_TYPE);
        data.append('engagement', SEC_DD_ENGAGEMENT_ID);
        data.append('group_by', SEC_DD_GROUP_BY);
        data.append('scan_date', now);
        data.append('active', 'true');
        data.append('verified', 'false');
        data.append('close_old_findings', 'true');
        data.append('deduplication_on_engagement', 'true');

        return instance.post('/api/v2/import-scan/', data, {
            headers: {
                ...data.getHeaders()
            }
        });
    }
};
